from pylab import *
from sklearn.naive_bayes import GaussianNB
from numpy.random import multivariate_normal




#generate data
mu0 = np.array([0, 0])
sigma = eye(2)
data = multivariate_normal(mu0 , sigma, size=500)
training_data = data[:400]
test_data = data[400:]
y_training = np.zeros(400)
y_test = np.zeros(100)

mu1 = np.array([20, 0])
data =multivariate_normal(mu1 , sigma, size=500)
training_data = np.vstack([training_data, data[:400]])
test_data = np.vstack([test_data, data[400:]])
y_training = np.hstack([y_training, np.ones(400)])
y_test = np.hstack([y_test, np.ones(100)])

x0_training, x1_training = training_data[:,0], training_data[:,1]
x0_test, x1_test = test_data[:,0], test_data[:,1]


fig, ax = plt.subplots()
ax.plot(x0_training[y_training==0], x1_training[y_training==0], 'o', label='category_0')
ax.plot(x0_training[y_training==1], x1_training[y_training==1], 'o', label='category_1')
ax.legend()
ax.set_xlabel('x0')
ax.set_ylabel('x1')
ax.set_title('Training Data')

#fit data
gnb = GaussianNB()
gnb.fit(data, y)

#plot gaussians
mu0 = gnb.theta_[0,:]
mu1 = gnb.theta_[1,:]
sigma0 = gnb.sigma_[0,:]
sigma1 = gnb.sigma_[1,:]

x0_range = np.arange(-5, 30, 0.01)
x1_range = np.arange(-5, 5, 0.01)
X0, X1 = np.meshgrid(x0_range, x1_range)

normal0 = bivariate_normal(X0, X1, sigmax=sigma0[0], sigmay=sigma0[1], mux=mu0[0], muy=mu0[1], sigmaxy=0.0)
normal1 = bivariate_normal(X0, X1, sigmax=sigma1[0], sigmay=sigma1[1], mux=mu1[0], muy=mu1[1], sigmaxy=0.0)

fig, ax = plt.subplots()
ax.plot(x0_test, x1_test, 'o', alpha=0.3)
ax.contour(X0, X1, normal0*normal1)
#ax.contour(X0, X1, normal1)
ax.set_xlabel('x0')
ax.set_ylabel('x1')
ax.set_title('Test Data and Contour Plot of the Two Fitted Gaussians')

#predict and plot decision surface
y_predict = gnb.predict(test_data)

fig, ax = plt.subplots()
ax.contour(X0, X1, normal1-normal0, [0], label='decision curve')
ax.plot(x0_test[y_predict==0], x1_test[y_predict==0], 'o', label='category_0')
ax.plot(x0_test[y_predict==1], x1_test[y_predict==1], 'o', label='category_1')
ax.legend()
ax.set_xlabel('x0')
ax.set_ylabel('x1')
ax.set_title('Test Data and the Decision Curve')