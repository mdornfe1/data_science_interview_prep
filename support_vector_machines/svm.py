from pylab import *
from sklearn import datasets, svm

iris = datasets.load_iris()
y = iris.target
data = iris.data[:,:2][np.logical_or(y==0, y==1)]
y = y[np.logical_or(y==0, y==1)]
x0 = data[:,0]
x1 = data[:,1]

clf = svm.SVC(kernel='linear', C = 1)
clf.fit(data, y)
theta0 = clf.coef0
theta = clf.coef_[0]
x0_decision = np.arange(4, 7, 0.1)
x1_decision = -theta[0] / theta[1] * x0_decision 

fig, ax = subplots()
ax.plot(x0[y==0], x1[y==0], 'o', label='setosa')
ax.plot(x0[y==1], x1[y==1], 'o', label='versicolor')
ax.plot(x0_decision, x1_decision, label='decision boundary')
ax.set_xlabel('sepal length (cm)')
ax.set_ylabel('sepal width (cm)')
ax.legend()
ax.set_title('Example of linearly separable data')


