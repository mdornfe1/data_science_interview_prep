\documentclass[12pt, letter]{report}

\usepackage{amsmath}    % need for subequations
\usepackage[pdftex]{graphicx}   % need for figures
\usepackage{verbatim}   % useful for program listings
\usepackage{color}      % use if color is used in text
\usepackage{subfigure}  % use for side-by-side figures
\usepackage{hyperref}   % use for hypertext links, including those to external documents and URLs
\begin{document}
\section{Bayesian Inference}
\subsection{Basic Idea}
In classical statistic \textit{Truth} is fixed. Observations are considered to be drawn at random from and underlying distribution, which represents the true nature of the system. In Bayesian statistics, on the other hand, what we understand as \textit{Truth} is constantly updated based on observations we make about the world. Bayes' rule states how to revise probability statements about the true nature of the world based on \textit{Data}. 
\begin{equation}
p(A \vert B) = \frac{p(B \vert A) p(A)}{p(B)}
\end{equation}
Here, $A$ and $B$ are statements about the world. $p(A)$ and $p(B)$ are the probabilities of observing $A$ and $B$ respectively. $p(A \vert B)$ is a conditional probability. It is the probability of observing $A$ given we've already observed $B$. 

In an experiment, we can consider $B$ to the be the \textit{Data} we observe and $A$ to be the true nature of the system were experimenting on. We use observation $B$ to make probability statements about $A$. That's what science is! In this language Bayes' rule can be written as
\begin{equation}
p(\text{\textit{Truth}} \vert \text{\textit{Data}}) = \frac{p(\text{\textit{Data}} \vert \text{\textit{Truth}}) p(\text{\textit{Truth}})}{p(\text{\textit{Data}})}
\end{equation}
These probabilities also have special names.
\begin{itemize}
\item $p(A)$ is the prior distribution. It is what we know about $A$ before we start the experiment. Often times this will come from a previous experiment, or our best guest at the outcome of the experiment.
\item $p(B)$ doesn't have a special name, but it is the probability of observing \textit{Data} in our experiment. We calculate it from the outcomes of the experiment.
\item $p(B \vert A)$ is called the likelihood of observing \textit{Data} $B$ given \textit{Truth} $A$. In practice, we need to assume the form of this (i.e. $A$ is a Gaussian or Bernoulli process).
\item $p(A \vert B)$ is call the posterior distribution. After the experiment is done, and we've collected \textit{Data} $B$, we update our knowledge of the \textit{Truth} $A$.
\end{itemize}

\subsection{An Example: The Monty Hall Problem}
In the Monty Hall problem a contestant is on a game show and has the opportunity to choose one of three doors. Behind two doors there are goats and behind one door is a car. If they successfully find the car they get to go home with it. The contestant chooses a door but does not open it. At this point the host opens one of the other doors to reveal a goat. The contestant then has the opportunity to switch doors. Should they switch?

We can use Bayesian inference to show that they should. Let's write out the different possibilities for the car and goat combinations.
\begin{equation}
\begin{aligned}
A_1 &= \text{car}, \text{goat}, \text{goat} \\
A_2 &= \text{goat}, \text{car}, \text{goat} \\
A_3 &= \text{goat}, \text{goat}, \text{car}
\end{aligned}
\end{equation}
So $A_i$ is the statement the car is behind door $i$. Without loss of generality let's assume the contestant initially chooses door $1$. Let's also define the statement
\begin{equation}
B_i = \text{The host reveals a goat behind door i}
\end{equation}
Without loss of generality less consider the case where the host reveals door $3$. Let's show the contestant has a better chance of finding the car if they switch to door $2$. The first column of the below table shows the prior probability distribution before the host reveals a door. Initially we have no extra info about the situation, so each configuration is equally probably. The \textit{Data} in this experiment is the door the host reveals. The second column shows the probability of different data measurements in this experiment. The contestant chose door $1$ so we know the host won't reveal door $1$. There's a $50/50$ chance the host reveals doors $2$ or $3$. Now we're considering the case in which the host reveals a goat behind door $3$. The third column shows the likelihood the host reveals a goat behind door 3 conditioned on the possible \textit{True} states of the system. We know there is no car behind door $3$ so the third row is $0$. If the $A_1$ is the true goat/car combination then there is a $0.5$ probability the host reveals door $3$. However, this is the important fact, if $A_2$ is the true goat/car combination there is $1.0$ probability the host reveals door $3$. The last column shows the posterior probability distribution calculated from the first three, using Bayes' law. We can see there is $0.5$ probability the car is behind door $2$ and only a $\frac{1}{3}$ probability the car is behind door $1$. The contestant should switch! 
\begin{center}
\begin{tabular} {c | c | c | c}
prior & Data & likelihood & posterior \\
\hline
$p(A_1)=\frac{1}{3}$ & $p(B_1)=0$ & $p(B_3 \vert A_1) = \frac{1}{2}$ & $p(A_1 \vert B_3)=\frac{(1/2)(1/3)}{1/2}=\frac{1}{3}$ \\
$p(A_2)=\frac{1}{3}$ & $p(B_2)=\frac{1}{2}$ & $p(B_3 \vert A_2) = 1$ & $p(A_2 \vert B_3)=\frac{(1)(1/3)}{1/2}=\frac{2}{3}$\\
$p(A_3)=\frac{1}{3}$ & $p(B_3)=\frac{1}{2}$ & $p(B_3 \vert A_3) = 0 $ & $p(A_3 \vert B_3)=\frac{(1)(0)}{1/2}=\frac{2}{3}=0$
\end{tabular}
\end{center}
\section{Naive Bayes Classifiers}
\subsection{Basic Idea}
Naive Bayes Classifiers are an application of Bayesian inference, in which Bayes' rule is used to assign objects to categories based on training data. The problem goes as follows: let's say you have a bunch of objects ${O_1, ..., O_N}$ with features $\textbf{x}_n=(x_{1} , ... , x_{M})$. The objects could be anything, pieces of furniture in the room for example. The features $\textbf{x}_n$ are the data we collect about object $O_n$. Examples of possible features are the size, color, and shape of the pieces of furniture. Our task is to teach a computer to identify a piece of furniture based on the measured features. That is based on a features $x_n$ can the computer identify object $O_n$ as a chair, table, couch, etc. Let's call these different possible classification categories ${C_1, ..., C_K}$. Naive Bayes classifiers use Bayesian inference to perform this categorization by calculating the posterior distribution
\begin{equation}
\label{eq:posterior}
p(C_k | x_1, ..., x_M),
\end{equation}
using maximum a posteriori estimation (I'll write more about this below). We then have the probability that an object with features $\textbf{x}_n$ is in category $C_k$. The object is then assigned to the category which maximizes the posterior probability (Eq. \ref{eq:posterior}). In the next subsection I'll write about the additional assumptions you need to place on the distribution in order to be able to do this calculation.  

\subsection{Conditional Independence of the Features}
Using Bayes' theorem we can write the posterior distribution (Eq. \ref{eq:posterior}) as 
\begin{equation} 
\label{eq:posterior2}
p(C_k | x_1, ..., x_M) = \frac{p(C_k) p(x_1, ... , x_M | C_k)}{p(x_1, ... , x_M)}
\end{equation}
In the language of the background section on Bayesian inference this can be written as
\begin{equation}
\text{posterior} = \frac{\text{prior} \times \text{likelihood}}{\text{data}}
\end{equation}
The denominator of Eq. \ref{eq:posterior2} does not depend on the cluster $C_k$, so for the purposes of classification we can focus on its numerator. Using Bayes' again the numerator can be written as $p(x_1, ... , x_M , C_k)$. This is the joint distribution of ${x_1, ... , x_M , C_k}$. Now repeatedly applying Bayes' rule to the joint distribution it can be written as
\begin{equation}
\label{eq:joint1}
\begin{aligned}
p(x_1, \dots, x_n, C_k) & = p(x_1 \vert x_2, \dots, x_n, C_k) p(x_2, \dots, x_n, C_k) \\
& = p(x_1 \vert x_2, \dots, x_n, C_k) p(x_2 \vert x_3, \dots, x_n, C_k) p(x_3, \dots, x_n, C_k) \\
& = \dots \\
& = p(x_1 \vert x_2, \dots, x_n, C_k) p(x_2 \vert x_3, \dots, x_n, C_k) \dots   p(x_{n-1} \vert x_n, C_k) p(x_n \vert C_k) p(C_k) \\
\end{aligned}
\end{equation}
Now we can introduce the assumption of conditional independence. Basically this says that a feature $x_i$ is conditionally independent of another feature $x_j$ given a category $C_k$. Symbolically this means that if $C_k$ is to the right of the vertical bar in a conditional probability distribution we can ignore any features $x_j$ to the right of the vertical bar in the conditional distribution. Thus, Eq. \ref{eq:joint} can be written as
\begin{equation} 
\label{eq:joint2}
\begin{aligned}
p(x_1, \dots, x_n, C_k) &= p(C_k)  p(x_1 \vert C_k)  p(x_2\vert C_k)  p(x_3\vert C_k) \cdots \\
&=p(C_k) \prod_{i=1}^M p(x_i \vert C_k)
\end{aligned}
\end{equation}
When you think about it this is actually a pretty bad assumption. Just because you know a piece of furniture in the room is a couch, there is no reason to believe that it's size and shape are independent of each other. Considering this, it's somewhat of a surprise to machine learning practitioners that Naive Bayes works so well.

\subsection{The Decision Rule: Maximizing the Posterior Probability}
Plugging Eq. \ref{eq:joint2} into Eq. \ref{eq:posterior2},
\begin{equation} 
\label{eq:posterior3}
p(C_k | x_1, ..., x_M) = \frac{p(C_k) \prod_{i=1}^M p(x_i \vert C_k)}{p(x_1, ... , x_M)}
\end{equation}
We still we need a way to decide how to assign object with features $x_i$ to a category $C_k$. An accepted way to do this is we choose the category $C_k$ which maximizes the posterior probability \ref{eq:posterior3}. Thus, the predicted category $\hat{y}$ can be expressed as 
\begin{equation} 
\label{eq:decision}
\hat{y} = \underset{k \in \{1, \dots, K\}}{\operatorname{argmax}} \ p(C_k) \displaystyle\prod_{i=1}^n p(x_i \vert C_k)
\end{equation}

\subsection{Calculating the Priors the Likelihoods: Gaussian Naive Bayes}
So now we have a rule we can use to assign objects to categories. However, both the prior distribution $p(C_k)$ and likelihood function $p(x_i \vert C_k)$ appear in the decision rule Eq. \ref{eq:decision}. We have to know the form of these functions to use the decision rule. The prior is calculated by counting up the number of objects in each class in the training set and dividing by the total number of objects. For example if we have 100 pieces of furniture in our training set, and there are two classes: $C_1 = \text{chairs}$ and $C_2 = \text{couches}$. Let's say there are 80 chairs and 20 couches. Then $p(C_1)=0.8$ and $p(C_2)=0.2$.

Calculating the likelihood is a little more involved, since we need to assume a form for the likelihoods $p(x_i \vert C_k)$. A common approach is to assume the likelihood is a Gaussian distribution.
\begin{equation}
\label{eq:likelihood}
p(x_i \vert C_k) = \frac{1}{\sqrt{2 \pi \sigma_{i,c}^2}} e^{- \frac{\left(x_i - \mu_{i,k} \right)^2}{2 \sigma_{i, k}^2}}
\end{equation}
This likelihood function has two parameters for each possible feature and category, the mean $\mu_{i,c}$ and the standard deviation $\sigma_{i,k}$. Training consists of calculating the mean and standard deviation from the training data. For example if feature $x_1 = \text{size}$ and the average size of the chairs in the training set is $5$ and the average size of the couches in the training set is $10$, then $\mu_{1,1}=5$ and $\mu_{1,2}=10$. Once we calculate the prior distribution and the parameters of the likelihood function from the training set we can use \ref{eq:decision} to perform the categorization of the objects 

\subsection{Example}
In this section I'm going to talk about an example with artificially generated data. The code for this example is available in gaussian\_nb.py in the this folder of the git repo. Fig \ref{fig:training} shows the artificially generated training data. It has two features $x_0$ and $x_1$ and two category labels $0$ and $1$. The data in category $0$ is drawn from a bivariate Gaussian distribution with mean vector $\boldsymbol{\mu}=(0,0)$ and covariance matrix $\boldsymbol{\Sigma}=I$. The training data for category $2$ is drawn from a bivariate Gaussian distribution with the same covariance matrix and $\boldsymbol{\mu}=(20,0)$.
\begin{figure}
\label{fig:training}
\centering
\includegraphics[width=0.5\linewidth]{training.png}
\end{figure}

The GaussianNB class in sklearn fits the data to Eq. \ref{eq:likelihood} by calculating the mean and standard deviation parameters from the training data. The GuassianNB class then predicts the category of the test examples by assigning the example to the category which maximizes Eq. \ref{eq:posterior3}. Fig. \ref{fig:test_contour} shows a plot of the test data with an overlay of the fitted Gaussians for both categories. The test data is assigned to the Gaussian which has stronger influence over it. The decision boundary is the intersection of these two Gaussians. Fig. \ref{fig:test_decision} shows the test data labeled by a cluster and a plot of the decision boundary.
\begin{figure}
\label{fig:test_contour}
\centering
\includegraphics[width=0.5\linewidth]{test_data_contour.png}
\end{figure}
\begin{figure}
\label{fig:test_decision}
\centering
\includegraphics[width=0.5\linewidth]{test_decision.png}
\end{figure}


\end{document}