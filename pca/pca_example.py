from pylab import *
from sklearn.decomposition import PCA

#generate data
x1 = 10 * np.random.rand(100)+5
x2 = x1 + 0.5 * randn(100)
X = np.vstack((x1,x2)).T
X = X - X.mean(axis=0)

Sigma = np.cov(X.T)
D, E = np.linalg.eig(Sigma)
p1 = E[1,1]/E[0,1]


plt.plot(X[:,0], X[:,1], 'o')
plt.plot(p1 * np.arange(-1,1,0.1))

#

pca = PCA(n_components=1)
pca.fit(X)
X_r = pca.transform(X)