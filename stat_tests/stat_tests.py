def sample_std(x):
	mu_x = np.mean(x)
	return sum((x-mu_x)**2) / (len(x) - 1)


def ind_t_statistic(x, y):
	mu_x = np.mean(x) ; mu_y = np.mean(y)
	sigma_x = sample_std(x) ; sigma_y = sample_std(y) 
	t = (mu_x - mu_y) / np.sqrt(sigma_x**2 + sigma_y**2) / sqrt(len(x))

	return t

x = stats.norm.rvs(loc=5,scale=10,size=500)
y = stats.norm.rvs(loc=5,scale=10,size=500)
t = ind_t_statistic(x, y)
stats.ttest_ind(x, y)