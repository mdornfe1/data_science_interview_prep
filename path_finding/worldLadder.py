from pythonds.graphs import Graph
from pythonds.basic import Queue
from numpy import inf

def bfs(graph, root):
	for key in graph.getVertices():
		vert = graph.getVertex(key)
		vert.setDistance(inf)
		vert.setPred(None)

	vertQueue = Queue()
	root.setDistance(0)
	vertQueue.enqueue(root)

	while (vertQueue.size() > 0):
		current = vertQueue.dequeue()
		print(current.id)

		for nbr in current.getConnections():
			if nbr.getDistance() == inf:
				nbr.setDistance( current.getDistance() + 1 )
				nbr.setPred( current )
				vertQueue.enqueue(nbr)

	return graph

def traverse(end):
	x = end
	while x.getPred():
		print( x.getId() )
		x = x.getPred()

	print( x.getId() )


start = graph.getVertex('buoy')
graph = bfs(graph, start)
end = graph.getVertex('Elsa')
traverse(end)





def buildGraph(wordList):
	d = {}
	g = Graph()
	for word in wordList:
		for i in range(len(word)):
			bucket = word[:i] + '_' + word[i+1:]
			if bucket in d:
				d[bucket].append(word)
			else:
				d[bucket] = [word]

	for bucket in d.keys():
		for word in d[bucket]:
			for word2 in d[bucket]:
				if word != word2:
					g.addEdge(word, word2)

	return g

def get4LetterWords():
	wordList = open('/usr/share/dict/words').read().split()

	return [word for word in wordList if len(word)==4]

wordList = get4LetterWords()
g = buildGraph(wordList)