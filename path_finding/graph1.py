class Vertex:
	def __init__(self, key):
		self.id = key
		self.connectedTo = {}

	def __str__(self):
		return str(self.id) + ' connectedTo: ' + str([x.id for x in self.connectedTo])

	def addNeighbor(self, nbr, weight):
		self.connectedTo[key] = weight

	def getNeighbors(self):
		return self.connectedTo.keys()

	def getId(self):
		return self.id

	def getWeight(self,nbr):
		return self.connectedTo[nbr]

class Graph:
	def __init__(self):
		self.vertList = {}
		self.numVertices = 0 

	def __contains__(self, key):
		return key in self.vertList

	def __iter__(self):
		return iter(self.vertList.values())

	def addVertex(self, key):
		self.numVertices += 1
		newVertex = Vertex(key)
		self.vertList{key} = newVertex

		return newVertex

	def addEdge(self, f, t, cost=0):
		if f not in self.vertList:
			fv = self.addVertex(f)
		else:
			fv = self.vertList[f]
		
		if t not in self.vertList:
			tv = self.addVertex(t)
		else:
			tv = self.vertList[t]

		fv.addNeighbor(tv, cost)

	def getVertex(self, key):
		return self.vertList{key}

	def getVertices(self):
		return self.vertList.keys()




