from pythonds.graphs import Graph, Vertex
from pythonds.basic import Queue

def buildGraph(wordFile):
	d = {}
	g = Graph()
	wfile = open(wordFile,'r')
	# create buckets of words that differ by one letter
	for line in wfile:
		word = line[:-1]
		for i in range(len(word)):
			bucket = word[:i] + '_' + word[i+1:]
			if bucket in d:
				d[bucket].append(word)
			else:
				d[bucket] = [word]
	# add vertices and edges for words in the same bucket
	for bucket in d.keys():
		for word1 in d[bucket]:
			for word2 in d[bucket]:
				if word1 != word2:
					g.addEdge(word1,word2)
	return g

def bfs(g, start):
  start.setDistance(0)
  start.setPred(None)
  vertQueue = Queue()
  vertQueue.enqueue(start)
  while (vertQueue.size() > 0):
	currentVert = vertQueue.dequeue()
	verts = [vert.id for vert in currentVert.getConnections()]
	print('currentVert ', currentVert.id, 'color', currentVert.color)
	print('connections ', verts)
	for nbr in currentVert.getConnections():
	  if (nbr.getColor() == 'white'):
		nbr.setColor('gray')
		nbr.setDistance(currentVert.getDistance() + 1)
		nbr.setPred(currentVert)
		vertQueue.enqueue(nbr)
	currentVert.setColor('black')

graph = buildGraph('words.txt')
start = graph.getVertex('fling')
bfs(graph, start)

verts = graph.getVertices()
[graph.getVertex(vert).setDistance(np.inf) for vert in verts]
vertQueue = Queue()
start.setDistance(0)
vertQueue.enqueue(start)
while (vertQueue.size() > 0):
	current = vertQueue.dequeue()
	print(current.id, current.getDistance())
	for node in current.getConnections():
		if node.getDistance() == np.inf:
			node.setDistance(current.getDistance() + 1)
			node.setPred = current
			vertQueue.enqueue(node) 
