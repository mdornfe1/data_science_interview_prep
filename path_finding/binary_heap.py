from math import log, ceil

class BinaryHeap():
	def __init__(self):
		self.hlist = [0]
		self.size = 0

	def print_tree(self):
		depth = int(ceil(log(self.size, 2)))
		for n in range(depth):
			print(self.hlist[2**n:2**(n+1)])

	def perc_up(self, i):
		while i / 2 > 0:
			if self.hlist[i] < self.hlist[i/2]:
				self.hlist[i/2] , self.hlist[i] = self.hlist[i] , self.hlist[i/2]

			i = i / 2 

	def perc_down(self, i):
		while 2 * i < self.size:
			mc = self.min_child(i)
			if self.hlist[i] > self.hlist[mc]:
				self.hlist[i], self.hlist[mc] = self.hlist[mc], self.hlist[i] 
			i = mc

	def min_child(self, i):
		if 2 * i + 1 > self.size:
			return 2 * i
		elif self.hlist[2*i] < self.hlist[2*i+1]:
			return 2 * i
		else:
			return 2 * i + 1

	def del_min(self):
		old_min = self.hlist[1]
		self.hlist[1] = self.hlist[self.size]
		self.size -= 1
		self.hlist.pop()
		self.perc_down(1)

		return old_min

	def insert(self, key):
		self.hlist.append(key)
		self.size += 1
		self.perc_up(self.size)

bh = BinaryHeap()
bh.insert(1)
bh.insert(5)
bh.insert(7)
bh.insert(3)
bh.insert(10)
bh.insert(20)
bh.insert(4)
bh.insert(223)
bh.insert(22)
bh.insert(25)
bh.insert(1)
bh.insert(45)
bh.print_tree()