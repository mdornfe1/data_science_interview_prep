import string

class BinaryTree:
	def __init__(self, key):
		"""
		Initiate tree with key and empty child nodes
		"""
		self.key = key
		self.left = None
		self.right = None

	def __str__(self):
		return self.key

	def insert_left(self, key):
		"""
		Insert new left child to root of tree. If the roots has no left
		child create a new tree and insert it into left child. If the tree
		does have a left child create new tree, assign old left child to
		left child of new tree. Assign new tree 
		"""
		if self.left is None:
			self.left = BinaryTree(key)
		else:
			t = BinaryTree(key)
			t.left = self.left
			self.left = t

	def insert_right(self, key):

		if self.right is None:
			self.right = BinaryTree(key)
		else:
			t = BinaryTree(key)
			t.right = self.right
			self.right = t

	def get_right(self):
		return self.right

	def get_left(self):
		return self.left

	def set_root_key(self, key):
		self.key = key

	def get_root_key(self):
		return self.key

def preorder(tree):
    if tree:
        print(tree.get_root_key())
        preorder(tree.get_left())
        preorder(tree.get_right())

"""
If the current token is a '(', add a new node as the left child of the current node, and descend to the left child.

If the current token is in the list ['+','-','/','*'], 
set the root value of the current node to the operator represented by 
the current token. Add a new node as the right child of the current 
ode and descend to the right child.

If the current token is a number, set the root value of the 
current node to the number and return to the parent.

If the current token is a ')', go to the parent of the current node.
"""

operators = ['+', '-', '/', '*']
symbols = [s for s in string.ascii_lowercase] + [str(n) for n in range(10)]
expression = '( 5 * ( 2 * x + 4 ) + 3 )'
tokenized = expression.split(' ')
parse_tree = BinaryTree('')
current_tree = parse_tree
parent_stack = []
parent_stack.append(current_tree)

for token in tokenized:
	if token == '(':
		current_tree.insert_left('')
		parent_stack.append(current_tree)
		current_tree = current_tree.get_left()
	elif token in operators:
		current_tree.set_root_key(token)
		current_tree.insert_right('')
		parent_stack.append(current_tree)
		current_tree = current_tree.get_right()
	elif token in symbols:
		current_tree.set_root_key(token)
		current_tree = parent_stack.pop()
	elif token == ')':
		current_tree = parent_stack.pop()
	else:
		raise ValueError

preorder(current_tree)






