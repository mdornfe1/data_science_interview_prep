from pythonds.graphs import Graph, Vertex
from pythonds.basic import Stack
from numpy import inf

def knightGraph(bdSize):
	ktGraph = Graph()
	for row in range(bdSize):
		for col in range(bdSize):
			source = posToNodeId(row, col, bdSize)
			newPositions = genLegalMoves(row, col, bdSize)
			for e in newPositions:
				target = posToNodeId( e[0], e[1], bdSize)
				ktGraph.addEdge(source, target)

	return ktGraph

def posToNodeId(row, col, bdSize):
	return row * bdSize + col

def nodeIdToPos(nodeId, bdSize):
	return nodeId / bdSize, nodeId % bdSize

def genLegalMoves(x, y, bdSize):
	newMoves = []
	moveOffsets = (
		[(-1,-2),(-1,2),(-2,-1),(-2,1), 
		( 1,-2),( 1,2),( 2,-1),( 2,1)])
	for i in moveOffsets:
		newX = x + i[0]
		newY = y + i[0]
		if legalCoord(newX, bdSize) and legalCoord(newY, bdSize):
			newMoves.append( (newX, newY) )

	return newMoves

def legalCoord(x, bdSize):
	if x >= 0 and x < bdSize:
		return True

	else:
		return False

def dfs(graph, root):
	[ktGraph.getVertex(key).setColor('white') for key in ktGraph.getVertices()]
	vertStack = []
	vertStack.append(root)
	while len(vertStack) > 0 :
		vert = vertStack.pop()
		print(len(vert.getConnections()))
		if vert.getColor() == 'white':
			vert.setColor('gray')
			for nbr in vert.getConnections(): 
				if nbr.getColor() == 'white':
					nbr.setColor('gray')
					vertStack.append(nbr)

def knightTour(n, path, u, limit):
	print(nodeIdToPos(u.id, 8))
	u.setColor('gray')
	path.append(u) #stack
	if n < limit:
		nbrList = list( u.getConnections() )
		i = 0
		done = False
		while i < len(nbrList) and not done:
			if nbrList[i].getColor() == 'white':
				done = knightTour(n+1, path, nbrList[i], limit)

			i += 1

		if not done:
			path.pop()
			u.setColor('white')
		else:
			done = True

		return done


if __name__ == '__main__':
	ktGraph = knightGraph(8)
	[ktGraph.getVertex(key).setColor('white') for key in ktGraph.getVertices()]
	root = ktGraph.getVertex(0)
	knightTour(0, [], root, 6)
	