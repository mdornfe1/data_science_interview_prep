class BinarySearchTree:
	def __init__(self):
		self.root = None
		self.size = 0

	def __str__(self):
		pass

	def __len__(self):
		return self.size

	def __iter__(self):
		return self.root.__iter__()

	def __setitem__(self, key, value):
		self.put(key, value)

	def __getitem__(self, key):
		return self.get(key)

	def __contains__(self, key):
		if self._get(key, self.root):
			return True
		else:
			return False

	def __delitem__(self,key):
	    self.delete(key)

	def put(self, key, val):
		if self.root is None:
			self.root = TreeNode(key, val)
		else:
			self._put(key, val, self.root)
			
		self.size = self.size + 1

	def _put(self, key, val, current):
		if key < current.key:
			if current.left is None:
				current.left = TreeNode(key, val, parent=current)
			else:
				self._put(key, val, current.left)
		else:
			if current.right is None:
			   current.right = TreeNode(key, val, parent=current)
			else:
				self._put(key, val, current.right)

	def get(self, key):
		if self.root is None: 
			return None
		else:
			result = self._get(key, self.root)
			if result:
				return result.val
			else:
				return None

	def _get(self, key, current):
		if current is None:
			return None
		elif current.key == key:
			return current
		elif key < current.key:
			return self._get(key, current.left)
		else:
			return self._get(key, current.right)

	def delete(self, key):
		if self.size > 1:
			node_to_remove = self._get(key, self.root)
			if node_to_remove is None:
				raise KeyError("Key is not in tree.")
			else:
				self.remove(node_to_remove)
				self.size -= 1

		elif self.size == 1 and self.root.key == key:
			self.root = None
			self.size -= 1

		else:
			raise KeyError("Key is not in tree.")

	def remove(self, node):
	if node.is_leaf():
		if node == node.parent.left:
			node.parent.left = None
	else:
		current.parent.right = None

	







class TreeNode:
	def __init__(self, key, val, left=None, right=None, parent=None):
		self.key = key
		self.val = val
		self.left = left
		self.right = right
		self.parent = parent

	def get_left(self):
		return self.left

	def get_right(self):
		return self.right

	def is_left(self):
		return self.parent and self.parent.left == self

	def is_right(self):
		return self.parent and self.parent.right == self

	def is_root(self):
		return self.parent is None

	def is_leaf(self):
		return self.left is None and self.right is None

	def has_child(self):
		return self.left or self.right

	def has_children(self):
		return self.left and self.right

	def replace_node(self, key, value, left, right):
		self.key = key
		self.value = value
		self.left = left
		self.right = right

		if self.get_left():
			self.left.parent = self
		if self.get_right():
			self.right.parent = self

bst = BinarySearchTree()
bst[10] = 'sdf'
bst[15] = 'sdsfsf'
bst[25] = 43
bst[1] = 12
