from pythonds.graphs import Graph

def knightGraph(bdSize):
	ktGraph = Graph()
	for row in range(bdSize):
		for col in range(bdSize):
			source = posToNodeId(row, col, bdSize)
			newPositions = genLegalMoves(row, col, bdSize)
			for e in newPositions:
				target = posToNodeId( e[0], e[1], bdSize)
				ktGraph.addEdge(source, target)

	return ktGraph

def posToNodeId(row, col, bdSize):
	return row * bdSize + col

def genLegalMoves(x, y, bdSize):
	newMoves = []
	moveOffsets = (
		[(-1,-2),(-1,2),(-2,-1),(-2,1), 
		( 1,-2),( 1,2),( 2,-1),( 2,1)])
	for i in moveOffsets:
		newX = x + i[0]
		newY = y + i[0]
		if legalCoord(newX, bdSize) and legalCoord(newY, bdSize):
			newMoves.append( (newX, newY) )

	return newMoves

def legalCoord(x, bdSize):
	if x >= 0 and x < bdSize:
		return True

	else:
		return False

if __name__ == '__main__':
	ktGraph = knightGraph(8)